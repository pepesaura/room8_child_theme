## WPCartagena Theme

WPCartagena is a twentyseventeen child based on _child (a WordPress Child Theme Boilerplate! by [Ahmad Awais](http://AhmadAwais.com/about/))

![wpcartagena](screenshot.jpg)


### License
- _child is licensed under GPL v2.0 and is distributed as is.
- wpcartagena is licensed under GPL v2.0 and is distributed as is.

---
#### Hay una discusión abierta en [gist.github.com/pepesaura / WPCartagena_theme.md](https://gist.github.com/pepesaura/5b579c59089e70d027127f2bebe1b0bc)
