### WPCartagena, un tema hijo, el padre de todos los _gists_
Usamos [Gist](https://gist.github.com/pepesaura/0a6434f06dbb8ec0ec84e2b7ec93457e/edit) como primer recurso para poder discutir sobre la creación de este tema hijo.


### Recursos

- [WordPress - Make WordPress Training](https://make.wordpress.org/training/handbook/theme-school/child-themes/child-themes-twentyseventeen/) - The WordPress training team creates downloadable lesson plans and related materials for instructors to use in live workshop environments.

### Enlaces en wpmudev

- [What’s New (and Awesome!) in WordPress 4.7](https://premium.wpmudev.org/blog/whats-new-wordpress-4-7/) - If you haven’t already checked out this new version of WordPress, here’s what you need to know.
- [How to Customize the Free Twenty Seventeen WordPress Theme](https://premium.wpmudev.org/blog/twenty-seventeen-wordpress-theme/) - This business-oriented theme marks a noteworthy departure from the blog-centric default themes of the past and reflects WordPress’ broader transition from a blogging platform into a platform well-suited to creating all sorts of websites.

- [The Complete Guide to the WordPress Theme Customizer](https://premium.wpmudev.org/blog/wordpress-theme-customizer-guide/) - WordPress 4.7 was released with a ton of great new features, including some user experience and user interface upgrades to the theme Customizer.

- [5 Hacks for Twenty Seventeen WordPress Child Theme](https://premium.wpmudev.org/blog/five-hacks-twenty-seventeen/) - In this post, I’ll show you five hacks you can use to make Twenty Seventeen uniquely your own. We’ll start with the basics and move on to more challenging and impactful customizations.
- [GitHub - 5 Hacks for Twenty Seventeen WordPress Child Theme](https://github.com/jpen365/five-hacks-for-twenty-seventeen) - This is an unfinished child theme intended to be used by developers and WordPress development students. It is a not a production-ready finished product. It is offered without any guarantee that it is bug-free or suitable for use by non-technical users.

### Otros recursos

- [Design Bombs: How To Master Twenty Seventeen (Or Any WordPress Theme in 2017)](https://www.designbombs.com/master-twenty-seventeen-wordpress-theme/) - In this tutorial, we will cover three different ways of changing the way your site looks and we’ll separate them into three different difficulty categories, from begginer to intermediate.
- [WPsnipp: 660+ WordPress Snippets, Code, Hacks, for your theme, blog](http://wpsnipp.com/) - Over 660+ WordPress snippets to enhance all aspects of your WordPress theme or blog wp snipp attracts web designers & developers world wide.

### Google Searches
- [Google Search: site:wp-snippets.com+add_action](https://www.google.es/search?q=site:wp-snippets.com+add_action)
- [Google Search: site:wpsnipp.com+add_action](https://www.google.es/search?q=site:wpsnipp.com+add_action)


### Otros recursos en GitHub

- [braginteractive/functions.php](https://gist.github.com/braginteractive/66b8f23856e3a036d9b0aa38fc15622c) - Gist: Twenty Seventeen Child WordPress Theme
- [ruthmaude/twentyseventeen-child](https://github.com/ruthmaude/twentyseventeen-child) - Repository: starter child theme for WordPress twentyseventeen theme
